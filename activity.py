# Copyright 2007-2008 One Laptop Per Child
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
import datetime
import gtk
import logging
import json
import math
import os
from gettext import gettext as _
from telepathy.interfaces import (
    CHANNEL_INTERFACE, CHANNEL_INTERFACE_GROUP, CHANNEL_TYPE_TEXT,
    CONN_INTERFACE_ALIASING)
from telepathy.constants import (
    CHANNEL_GROUP_FLAG_CHANNEL_SPECIFIC_HANDLES,
    CHANNEL_TEXT_MESSAGE_TYPE_NORMAL)
from telepathy.client import Connection, Channel
import time
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter

from sugar.graphics import style
from sugar.graphics.alert import NotifyAlert
from sugar.graphics.alert import Alert
from sugar.graphics.toolbutton import ToolButton

from sugar.graphics.toolbarbox import ToolbarBox
from sugar.activity import activity
from sugar.presence import presenceservice
from sugar.graphics.icon import Icon
from sugar.activity.widgets import ActivityToolbarButton
from sugar.activity.widgets import StopButton, ShareButton
from sugar.graphics.toolcombobox import ToolComboBox
from random import randint,choice
from chat import smilies
import operator
from chat.box import ChatBox
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas


logger = logging.getLogger('ChatStudioGroup-activity')
steps=0
SMILIES_COLUMNS = 5
snum=0
c1=randint(5,9)
chk=0
c2=randint(5,9)
scoretime=time.time()
flag=0
accuracy=0
cssw=0
no_of_mistake=0
lans=[]
players=[]
timescores=[]
accscores=[]
game_incomplete_players=[]
luans=[]
gameComplete=False
full_alert=True
ad=False
sb=False


class Visual1(ToolButton):

    def __init__(self, activity, **kwargs):
        ToolButton.__init__(self, 'scorestats', **kwargs)
        self.props.tooltip = _('Score Stats')
        self.connect('clicked', self.draw_bars1)


    def draw_bars1(self,button):
	global players
	global accscores
	win = gtk.Window()
	win.connect("destroy", lambda x: gtk.main_quit())
	win.set_default_size(830,450)
	win.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse('white'))
	win.set_title("Score Statistics")
	f = Figure(figsize=(5,4), dpi=100)
	a = f.add_subplot(111)
	alist=accscores
	dlist=players
	y_pos = np.arange(len(dlist))	
	error = np.random.rand(len(dlist))
	a.barh(y_pos, alist, xerr=error, align='center', alpha=0.4)
	a.set_yticks(y_pos)
	a.set_yticklabels(dlist)
	canvas = FigureCanvas(f)  # a gtk.DrawingArea
	win.add(canvas)
	win.show_all()
	gtk.main()


class Visual2(ToolButton):
    def __init__(self, activity, **kwargs):

        ToolButton.__init__(self, 'timestats', **kwargs)
        self.props.tooltip = _('Time Stats')
        self.connect('clicked', self.draw_bars1)


    def draw_bars1(self,button):
	win = gtk.Window()
	win.connect("destroy", lambda x: gtk.main_quit())
	win.set_default_size(830,450)
	win.modify_bg(gtk.STATE_NORMAL,gtk.gdk.color_parse('white'))
	win.set_title("Score Statistics")
	f = Figure(figsize=(5,4), dpi=100)
	a = f.add_subplot(111)
	global timescores
	global accscores
	alist=timescores
	dlist=players
	y_pos = np.arange(len(dlist))	
	error = np.random.rand(len(dlist))
	a.barh(y_pos, alist, xerr=error, align='center', alpha=0.4)
	a.set_yticks(y_pos)
	a.set_yticklabels(dlist)
	canvas = FigureCanvas(f)  # a gtk.DrawingArea
	win.add(canvas)
	win.show_all()
	gtk.main()

class scoreWindow:
    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
	self.window.set_position(gtk.WIN_POS_CENTER)
	#self.window.set_size_request(800, 200)
	self.window.set_default_size(600,300)
	self.window.set_title("Score card")
	self.vb=gtk.VBox()

	line=''#Declare an empty string 
	self.go=gtk.Label("Game Over\n")
        self.lalign = gtk.Alignment(0, 0, 0, 0)
        self.label_result = gtk.Label("  Rank\tAccuracy\t\tStart\t\tAdd\t\tMistakes\t\t\tSteps\t\tTime\t\tMode")
        #self.label_result.set_justify(gtk.JUSTIFY_LEFT)
        self.lalign.add(self.label_result)

	self.vb.pack_start(self.go, False, False, 0)
        self.vb.pack_start(self.lalign, False, False, 0)
	self.hb1=gtk.HBox()
	self.tv = gtk.TextView()
	self.tv.modify_text(gtk.STATE_NORMAL,gtk.gdk.color_parse('black'))
	self.tv.set_editable(0)
	self.tv.set_cursor_visible(0)
	self.tv.set_left_margin(30)
        textbuffer = self.tv.get_buffer()
        self.tv.show()

	global gameComplete
	global cssw
	if (cssw==0):
	 self.readscores(textbuffer,line)
	elif (cssw==1):
	 if (gameComplete):
	  self.readscores1(textbuffer,line)
	 else:
	  line='Incomplete Game'
          textbuffer.set_text(line)
	 cssw=0
	elif (cssw==2):
	 if (gameComplete):
	  self.readscores2(textbuffer,line)
	 else:
	  line='Incomplete Game'
          textbuffer.set_text(line)
	 cssw=0
	self.hb1.pack_start(self.tv, fill=False)
	self.hb2=gtk.HBox()
	self.b1=gtk.Button("Check Last Game answer")
	self.b2=gtk.Button("Last Game Score")
	self.b1.connect("clicked", self.chkans_card,self.window)
	self.b2.connect("clicked", self.last_game_score,self.window)
	self.hb2.pack_start(self.b1, fill=False)
	self.hb2.pack_start(self.b2, fill=False)
	self.vb.pack_start(self.hb1, fill=False)
	self.vb.pack_start(self.hb2, fill=False)
	color = gtk.gdk.color_parse('#FF8300')
        self.window.modify_bg(gtk.STATE_NORMAL, color)
	self.window.add(self.vb)
	self.window.show_all()

    """
    The following functions is invoked on click
    of "ScoreButton" of Toolbar
    
    """
    

    def readscores(self,textbuffer,line):
	 if not (os.stat("score_card1.txt").st_size==0): #when file not empty
          with open("score_card1.txt", "r") as infile:
	   for i in range(0,10): #displays only top 10 lines of the file
	     line+='\n'+str(i+1)+'\t'+infile.readline()
          textbuffer.set_text(line)

    """
    The following functions is invoked on click
    of "Check Last Game Answer"
    
    """
    def readscores2(self,textbuffer,line):
	 global luans
	 global lans
	 a=len(lans)
	 b=len(luans)
	 i,j=0,0
	 line+='Your Answer \t <------->\t Correct Answer\n'
	 while i < a and j<b:
	  line+=str(luans[i])+' \t\t\t <------->\t\t'+str(lans[j])+'\n'
	  i+=1
	  j+=1
         textbuffer.set_text(line)

    def readscores1(self,textbuffer,line):
	  line+='\n-\t\t'+str(accuracy)+'\t\t  '+str(c1)+'\t\t\t  '+str(c2)+'\t\t    '+str(no_of_mistake)+ \
	        '\t\t\t\t  '+str(steps)+'\t\t\t'+'%.1f' % scoretime+'\t\t\t'

	  if ad:
	   line+='Addition\n'
	  elif sb:
	   line+='Subtraction\n'
          textbuffer.set_text(line)

    def chkans_card(self,button,window):
	window.destroy()
	global cssw
	cssw=2
	scoreWindow()
	
    def last_game_score(self,button,window):
	window.destroy()
	global cssw
	cssw=1
	scoreWindow()


class ScoreButton(ToolButton):
    def __init__(self, activity, **kwargs):
        ToolButton.__init__(self, 'score_card', **kwargs)
        self.props.tooltip = _('Score Card')
        self.connect('clicked', self.__show_score_win, activity)

    def __show_score_win(self, button, activity):
	scoreWindow()



class NotifyAlert1(Alert):
    def __init__(self, **kwargs):
	Alert.__init__(self, **kwargs)
	icon = Icon(icon_name='dialog-ok')
        self.add_button(1, _('New\nGame'), icon=None)
	self.add_button(2, _(' Change \n Numbers '), icon=None)
        self.add_button(3, _('Easy'), icon=None)
        self.add_button(4, _('Medium'), icon=None)
        self.add_button(5, _('Hard'), icon=None)
	self.add_button(6, _(''), icon)
	icon.show()
        

class NotifyAlert2(Alert):
    def __init__(self, **kwargs):
	Alert.__init__(self, **kwargs)
        self.add_button(2, _(' Change \n Numbers '), icon=None)



# pylint: disable-msg=W0223
class ChatStudioGroup(activity.Activity):

    def __init__(self, handle):
        smilies.init()
        self.chatbox = ChatBox()
        super(ChatStudioGroup, self).__init__(handle)
        self.entry = None
        root = self.make_root()
        self.set_canvas(root)
        root.show_all()
	self.entry.set_sensitive(True)
        self.entry.grab_focus()

	self.hardmode=False
	global accuracy
	accuracy=0
	global luans
	del luans[:]
	self.buddylist=[]
	global lans
	del lans[:]
	global players
	global accscores
	global timescores
	global full_alert
	self.changedonce=False
	players[:]
	accscores[:]
	timescores[:]
	self.initialize=True
	self.create_toolbar()
	self.a1=randint(5,9)
	self.a2=randint(5,9)
	self.log=''
	self.a=[]
	self.aread=[]
	self.myacc=0
	self.chances=0
	self.gameover=True
	self.report_dict = Counter()
	self.publicflag=False
	self.index=0
	self.modecombo=False
	self.newgame=True
	self.v=0
        pservice = presenceservice.get_instance()
        self.owner = pservice.get_owner()
	self.buddylist.append(self.owner.props.nick)
        # Chat is room or one to one:
        self._chat_is_room = False
        self.text_channel = None


	#self.run_main_loop()
        if self.shared_activity:
            # we are joining the activity
            self.connect('joined', self._joined_cb)
            if self.get_shared():
                # we have already joined
                self._joined_cb(self)
		full_alert=False
        elif handle.uri:
            # XMPP non-Sugar incoming chat, not sharable
            share_button.props.visible = False
            self._one_to_one_connection(handle.uri)
        else:
            # we are creating the activity
            if not self.metadata or self.metadata.get('share-scope',
                    activity.SCOPE_PRIVATE) == activity.SCOPE_PRIVATE:
                # if we are in private session
                self._alert(_('Off-line'), _('Share, or invite someone.'))
            self.connect('shared', self._shared_cb)
	    full_alert=True
	
    def create_toolbar(self):
        toolbar_box = ToolbarBox()
        self.set_toolbar_box(toolbar_box)
        toolbar_box.toolbar.insert(ActivityToolbarButton(self), -1)
	self._modes = ToolComboBox()
        self._modelist = ['Select Mode','Add + ', 'Subtract - ']
       	for i, f in enumerate(self._modelist):
         self._modes.combo.append_item(i, f) 
       	self.modes_handle_id = self._modes.combo.connect("changed",self._changemodes_toolbar)
        toolbar_box.toolbar.insert(self._modes, -1)
        self._modes.combo.set_active(0)

	scoreButton=ScoreButton(self)
	toolbar_box.toolbar.insert(scoreButton,-1)

	scorestats=Visual1(self)
	toolbar_box.toolbar.insert(scorestats,-1)

	timestats=Visual2(self)
	toolbar_box.toolbar.insert(timestats,-1)

	share_button=ShareButton(self)
	toolbar_box.toolbar.insert(share_button,-1)

	stopButton=StopButton(self)
	toolbar_box.toolbar.insert(stopButton,-1)

        toolbar_box.show_all()
    
    def update_ad_anslist(self,x,y):
	 global lans
	 del lans[:]
	 if self.hardmode:
	  xsum=100
	  self.hardmode=False
	 else:
	  xsum=50
	 lans.append(x+y)
	 counter = 1
	 while(lans[-1]<=xsum):
	  lans.append(lans[counter-1]+y)
	  counter+=1

    def update_sb_anslist(self,x,y):
	 global lans
	 del lans[:]
	 lans.append(x-y)
	 counter = 1
	 while (lans[counter-1]>0):
	  val=lans[counter-1]-y
	  lans.append(val)
	  if ((val-y)>0):
	   counter+=1
	  else:
	   break

    def _create_pallete_smiley_table(self):
        row_count = int(math.ceil(len(smilies.THEME) / float(SMILIES_COLUMNS)))
        table = gtk.Table(rows=row_count, columns=SMILIES_COLUMNS)
        index = 0

        for y in range(row_count):
            for x in range(SMILIES_COLUMNS):
                if index >= len(smilies.THEME):
                    break
                path, hint, codes = smilies.THEME[index]
                image = gtk.image_new_from_file(path)
                button = gtk.ToolButton(icon_widget=image)
                button.set_tooltip(gtk.Tooltips(), codes[0] + ' ' + hint)
                button.connect('clicked', self._add_smiley_to_entry, codes[0])
                table.attach(button, x, x + 1, y, y + 1)
                button.show()
                index = index + 1
        return table

    def _add_smiley_to_entry(self, button, text):
        self._smiley.palette.popdown(True)
        pos = self.entry.props.cursor_position
        self.entry.insert_text(text, pos)
        self.entry.grab_focus()
        self.entry.set_position(pos + len(text))


    def _shared_cb(self, sender):
	global full_alert
        full_alert=True
	self.publicflag=True
        logger.debug('Chat was shared')
        self._setup()

    def _one_to_one_connection(self, tp_channel):
        """Handle a private invite from a non-Sugar XMPP client."""
        if self.shared_activity or self.text_channel:
            return
        bus_name, connection, channel = json.loads(tp_channel)
        logger.debug('GOT XMPP: %s %s %s', bus_name, connection,
                     channel)
        Connection(
            bus_name, connection, ready_handler=lambda conn: \
            self._one_to_one_connection_ready_cb(bus_name, channel, conn))

    def _one_to_one_connection_ready_cb(self, bus_name, channel, conn):
        """Callback for Connection for one to one connection"""
        text_channel = Channel(bus_name, channel)
        self.text_channel = TextChannelWrapper(text_channel, conn)
        self.text_channel.set_received_callback(self._received_cb)
        self.text_channel.handle_pending_messages()
        self.text_channel.set_closed_callback(self._one_to_one_connection_closed_cb)
        self._chat_is_room = False
        self._alert(_('On-line'), _('Private Chat'))
        # XXX How do we detect the sender going offline?

    def enable_entry(self):
	self.entry.set_sensitive(True)
        self.entry.grab_focus()
        self._smiley.props.sensitive = True
 
    def _changemodes_toolbar(self, combo):
        self.x = combo.get_active()
	global c1
	global c2
	self.modecombo=True
	if (self.x == 1):

	 c1=randint(5,9)
	 c2=randint(5,9)
	 combo.set_sensitive(False)
	 self.a1=c1
	 self.a2=c2
	 self.sum1=self.a1+self.a2
	 self.update_ad_anslist(c1,c2)
	 global ad
	 ad=True
	 if full_alert:
	  self.showalert()
	 else:
	  self.showalert2()
	 self.enable_entry()

 	elif (self.x==2):

	 c1=randint(50,55)
	 c2=randint(5,9)
	 combo.set_sensitive(False)
	 self.a1=c1
	 self.a2=c2
	 self.update_sb_anslist(c1,c2)
	 global sb
	 sb=True
	 if full_alert:
	  self.showalert()
	 else:
	  self.showalert2()
	 self.enable_entry()


    def _one_to_one_connection_closed_cb(self):
        """Callback for when the text channel closes."""
        self._alert(_('Off-line'), _('left the chat'))

    def _setup(self):
        self.text_channel = TextChannelWrapper(
            self.shared_activity.telepathy_text_chan,
            self.shared_activity.telepathy_conn)
        self.text_channel.set_received_callback(self._received_cb)
        self._alert(_('On-line'), _('Connected'))
        self.shared_activity.connect('buddy-joined', self._buddy_joined_cb)
        self.shared_activity.connect('buddy-left', self._buddy_left_cb)
        self._chat_is_room = True
	self.entry.set_sensitive(True)
        self.entry.grab_focus()
        self._smiley.props.sensitive = True

    def _joined_cb(self, sender):
        """Joined a shared activity."""
	global full_alert
        if not self.shared_activity:
            return
        logger.debug('Joined a shared chat')
        for buddy in self.shared_activity.get_joined_buddies():
            self._buddy_already_exists(buddy)
	full_alert=False
        self._setup()

    def _received_cb(self, buddy, text):
        """Show message that was received."""
        if buddy:
            if type(buddy) is dict:
                nick = buddy['nick']
            else:
                nick = buddy.props.nick
        else:
            nick = '???'
	val=buddy.props.nick
	if val not in self.buddylist:
	    self.buddylist.append(val)
        logger.debug('Received message from %s: %s', nick, text)
        self.chatbox.add_text(buddy, text)


    def _alert(self, title, text=None):
        alert = NotifyAlert(timeout=5)
        alert.props.title = title
        alert.props.msg = text
        self.add_alert(alert)
        alert.connect('response', self._alert_cancel_cb)
        alert.show()

    def _alert_cancel_cb(self, alert, response_id):
        self.remove_alert(alert)

    def _alert1(self, title, text=None):
        alert = NotifyAlert1()
        alert.props.title = title
        alert.props.msg = text
        self.add_alert(alert)
        alert.connect('response', self.ng)
        alert.show()

    def _alert2(self, title, text=None):
        alert = NotifyAlert2()
        alert.props.title = title
        alert.props.msg = text
        self.add_alert(alert)
        alert.connect('response', self.ng2)
        alert.show()

    def ng(self, alert, response_id):
	global steps
 	global no_of_mistake
	global accuracy
	global lans
	global luans
	global game_incomplete_players
	global gameComplete
	del self.a[:]
	del self.buddylist[:]
	self.buddylist.append(self.owner.props.nick)
	steps=0
        self.chatbox._chat_log = ''
 	accuracy=0
	no_of_mistake=0
	gameComplete=False
	self.index=0
	del luans[:]
	self.gameover=True
	global players
	global accscores
	global timescores
	del players[:]
	del accscores[:]
	del timescores[:]
	self.initialize=True
        self.remalert(alert)
	global c1
	global c2
	self.chatbox.rem()		
	if(response_id==1):
	 if ad:
	  c1=randint(5,9)	
	  c2=randint(5,9)
	 elif sb:
	  c1=randint(50,59)	
	  c2=randint(5,9)

	elif(response_id==2): #OK--> Change Numbers Dialog

	 messagedialog = gtk.MessageDialog(parent=None, flags=0, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_OK,\
	 message_format="Enter Numbers")
	 action_area = messagedialog.get_content_area()
	
	 lbl1=gtk.Label("Start")
	 entry1 = gtk.Entry()
         entry1.set_size_request(int(gtk.gdk.screen_width() / 25),-1)
	 if ad:
	  lbl2=gtk.Label("+Add")
	 elif sb:
	  lbl2=gtk.Label("+Subtract")
	 
	 entry2 = gtk.Entry()
         entry2.set_size_request(int(gtk.gdk.screen_width() / 25),-1)
	
	 action_area.pack_start(lbl1)
	 action_area.pack_start(entry1)
	 action_area.pack_start(lbl2)
	 action_area.pack_start(entry2)
	 messagedialog.show_all()
	 ark=messagedialog.run()

         if ark == gtk.RESPONSE_OK:
	  try:
       	   c1 = int(entry1.get_text())
	   c2 = int(entry2.get_text())
	   if sb:
	    if (c2>c1):
		#raise BadNum("Num2 canot be greater than Num1")
		raise Exception
	  except Exception:
	   if ad:
	    c1=randint(5,9)
	    c2=randint(5,9)

	   elif sb:
	    c1=randint(50,59)
	    c2=randint(5,9)

	 messagedialog.destroy()

	elif(response_id==3):
	 if ad:
	  c1=randint(1,9)	
	  c2=c1

	 elif sb:
	  easylistStartNum=[50,52,54,55,56,51]
	  l1=[6,9,3,2]
	  l2=[5,10,2]
	  l3=[7,8]
	  c1=choice(easylistStartNum)	
	  if (c1==50):
	   c2=choice(l2)
	  elif (c1==51):
	   c2=3
	  elif (c1==52):
	   c2=2
	  elif (c1==54):
	   c2=choice(l1)
	  elif (c1==55):
	   c2=5
	  elif (c1==56):
	   c2=choice(l3)


	elif(response_id==4):
	 l11=[51,52,53,54,56,57,58,59]
	 if ad:
	  c1=randint(2,9)
	  c2=randint(2,9)
	  if(c1==c2):
	   c2=randint(2,9)
	 elif sb:
	  c1=choice(l11)
	  c2=randint(2,9)
	  if(c1==c2):
	   c2=randint(2,9)

	 
	elif(response_id==5):
	 if ad:
	  c1=randint(5,10)	
	  c2=randint(5,10)
	 elif sb:
	  c1=randint(100,105)	
	  c2=randint(5,9)
	 self.hardmode=True

	elif(response_id==6):
	 self.report_scores_1()
	 if ad:
	  c1=randint(5,9)	
	  c2=randint(5,9)
	 elif sb:
	  c1=randint(50,59)	
	  c2=randint(5,9)

	self.a1=c1
	self.a2=c2
	if ad:
	 self.update_ad_anslist(self.a1,self.a2)
	elif sb:
	 self.update_sb_anslist(self.a1,self.a2)
	self.showalert()
	game_incomplete_players=[]

        
    def report_scores_1(self):	
         w1 = gtk.Window()
 	 w1.set_size_request(300, 400)
	 w1.set_resizable(False)
	 w1.set_title("Report")
	 line=''
	 line2=''
	 lalign1 = gtk.Alignment(0, 0, 0, 0)
         top_label = gtk.Label("  Rank  User      Score")
         lalign1.add(top_label)
	 hb1=gtk.VBox()
	 hb2=gtk.HBox()
	 hb3=gtk.HBox()
	 tv = gtk.TextView()
	 tv.modify_text(gtk.STATE_NORMAL,gtk.gdk.color_parse('black'))
	 tv.set_editable(0)
	 tv.set_cursor_visible(0)
         textbuffer = tv.get_buffer()
	 tv2 = gtk.TextView()
	 tv2.modify_text(gtk.STATE_NORMAL,gtk.gdk.color_parse('black'))
	 tv2.set_editable(0)
	 tv2.set_cursor_visible(0)
	 textbuffer2 = tv2.get_buffer()

	 c=1
	 global game_incomplete_players
	 for key,val in self.report_dict.most_common(): #displays ranks
	  if key in game_incomplete_players:
	   line+=" "+str(c)+'\t'+str(key)+" Left \n"
	  else:
	   line+=" "+str(c)+'\t'+str(key)+"\n"

	  line2+=" "+str(val)+"\n"
	  c+=1
	 textbuffer.set_text(line2)
	 textbuffer2.set_text(line)
         tv.show()
         tv2.show()
	 tv.modify_text(gtk.STATE_NORMAL,gtk.gdk.color_parse('red'))
	 tv2.modify_text(gtk.STATE_NORMAL,gtk.gdk.color_parse('blue'))

	 hb2.pack_start(tv2, fill=False)
	 hb2.pack_start(tv, fill=False)

	 hb1.pack_start(lalign1, fill=False)
	 hb1.pack_start(hb2, fill=False)
	 hb1.pack_start(hb3, fill=False)

	 w1.add(hb1)
	 color = gtk.gdk.color_parse('#FFFFFF')
         w1.modify_bg(gtk.STATE_NORMAL, color)

	 w1.set_position(gtk.WIN_POS_CENTER)
	 w1.show_all()

    def setmode(self, widget, data=None):
        modenum=widget.get_active()
	if (modenum==0):
	 self.hardmode=False
	elif (modenum==1):
	 self.hardmode=True


    def remalert(self,alert):
	self.remove_alert(alert)

    def showalert(self):
        a=0
	if ad:	
         self._alert1(_('\t\tStart : '+str(c1)+ '\n\t\t+ Add\t: '+str(c2)), _(''))
	elif sb:
         self._alert1(_('\t\tStart : '+str(c1)+ '\n\t\t- Subtract\t: '+str(c2)), _(''))

    def ng2(self, alert, response_id):
	global game_incomplete_players
	global steps
 	global no_of_mistake
	global accuracy
	global lans
	global luans
	global gameComplete
	del self.a[:]
	del self.buddylist[:]
	self.changedonce=True
	self.buddylist.append(self.owner.props.nick)
	game_incomplete_players=[]
	steps=0
        self.chatbox._chat_log = ''
 	accuracy=0
	no_of_mistake=0
	gameComplete=False
	self.index=0
	del luans[:]
	self.gameover=True
	global players
	global accscores
	global timescores
	del players[:]
	del accscores[:]
	del timescores[:]
	self.initialize=True
        self.remalert(alert)
	global c1
	global c2
	self.chatbox.rem()		
	if(response_id==1):
	 if ad:
	  c1=randint(5,9)	
	  c2=randint(5,9)
	  self.xsum=50
	 elif sb:
	  c1=randint(50,59)	
	  c2=randint(5,9)

	elif(response_id==2): #OK--> Change Numbers Dialog

	 messagedialog = gtk.MessageDialog(parent=None, flags=0, type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_OK,\
	 message_format="Enter Numbers")
	 action_area = messagedialog.get_content_area()
	
	 lbl1=gtk.Label("Start")
	 entry1 = gtk.Entry()
         entry1.set_size_request(int(gtk.gdk.screen_width() / 25),-1)
	 if ad:
	  lbl2=gtk.Label("+ Add")
	 elif sb:
	  lbl2=gtk.Label("- Subtract")
	 
	 entry2 = gtk.Entry()
         entry2.set_size_request(int(gtk.gdk.screen_width() / 25),-1)
	 f = gtk.Frame( "Select your mode") 
	 box = gtk.VBox()
	 f.add( box)
	 button1 = gtk.RadioButton(None, "Easy")
	 button1.connect("toggled", self.setmode, "Easy")
	 box.pack_start(button1, True, True, 0)

	 button2 = gtk.RadioButton(button1, "Hard")
	 button2.connect("toggled", self.setmode, "Hard")
	 box.pack_start(button2, True, True, 0)
         button2.show()

	 action_area.pack_start(lbl1)
	 action_area.pack_start(entry1)
	 action_area.pack_start(lbl2)
	 action_area.pack_start(entry2)
	 action_area.pack_start(f)

	 messagedialog.show_all()
	 ark=messagedialog.run()

         if ark == gtk.RESPONSE_OK:
	  try:
       	   c1 = int(entry1.get_text())
	   c2 = int(entry2.get_text())
	   if sb:
	    if (c2>c1):
		#raise BadNum("Num2 canot be greater than Num1")
		raise Exception
	  except Exception:
	   if ad:
	    c1=randint(5,9)
	    c2=randint(5,9)

	   elif sb:
	    c1=randint(50,59)
	    c2=randint(5,9)

	 messagedialog.destroy()

	self.a1=c1
	self.a2=c2
	if ad:
	 self.update_ad_anslist(self.a1,self.a2)
	elif sb:
	 self.update_sb_anslist(self.a1,self.a2)
	self.showalert2()

    def showalert2(self):
        a=0 	
	if self.changedonce:
	 if ad:	
          self._alert2(_('\t\tStart : '+str(c1)+ '\n\t\t+ Add\t: '+str(c2)), _(''))
	 elif sb:
          self._alert2(_('\t\tStart : '+str(c1)+ '\n\t\t- Subtract\t: '+str(c2)), _(''))
	else:
          self._alert2(_('\t\t '), _(''))

    def _buddy_joined_cb(self, sender, buddy):
        """Show a buddy who joined"""
        if buddy == self.owner:
            return
        self.chatbox.add_text(buddy,
                buddy.props.nick + ' ' + _('joined the chat'),
                status_message=True)
	 
    def _buddy_left_cb(self, sender, buddy):
        """Show a buddy who joined"""
        if buddy == self.owner:
            return
        self.chatbox.add_text(buddy,
                buddy.props.nick + ' ' + _('left the chat'),
                status_message=True)

    def _buddy_already_exists(self, buddy):
        """Show a buddy already in the chat."""
        if buddy == self.owner:
            return
        self.chatbox.add_text(buddy, buddy.props.nick + ' ' + _('is here'),
                status_message=True)

    def can_close(self):
        """Perform cleanup before closing.
        Close text channel of a one to one XMPP chat.
        """
	if self._chat_is_room is False:
            if self.text_channel is not None:
                self.text_channel.close()
        return True

    def make_root(self):
        entry = gtk.Entry()
        entry.modify_bg(gtk.STATE_INSENSITIVE,
                        style.COLOR_WHITE.get_gdk_color())
        entry.modify_base(gtk.STATE_INSENSITIVE,
                          style.COLOR_WHITE.get_gdk_color())
        entry.set_sensitive(False)
        entry.connect('activate', self.entry_activate_cb)
        entry.connect('key-press-event', self.entry_key_press_cb)
        self.entry = entry

        hbox = gtk.HBox()
        hbox.add(entry)

        box = gtk.VBox(homogeneous=False)
        box.pack_start(self.chatbox)
        box.pack_start(hbox, expand=False)

        return box


    def entry_key_press_cb(self, widget, event):
        """Check for scrolling keys.
        Check if the user pressed Page Up, Page Down, Home or End and
        scroll the window according the pressed key.
        """
        vadj = self.chatbox.get_vadjustment()
        if event.keyval == gtk.keysyms.Page_Down:
            value = vadj.get_value() + vadj.page_size
            if value > vadj.upper - vadj.page_size:
                value = vadj.upper - vadj.page_size
            vadj.set_value(value)
        elif event.keyval == gtk.keysyms.Page_Up:
            vadj.set_value(vadj.get_value() - vadj.page_size)
        elif event.keyval == gtk.keysyms.Home and \
             event.state & gtk.gdk.CONTROL_MASK:
            vadj.set_value(vadj.lower)
        elif event.keyval == gtk.keysyms.End and \
             event.state & gtk.gdk.CONTROL_MASK:
            vadj.set_value(vadj.upper - vadj.page_size)



    def entry_activate_cb(self, entry):

	strr="Please enter a number."
	self.chatbox._scroll_auto = True
        text = entry.props.text
	logger.debug('Entry: %s' % text)
 	global scoretime
	global accuracy
	global gameComplete
	 
        if text.isdigit():
	 self.chatbox.add_text(self.owner, text)
       	 entry.props.text = ''
         if self.text_channel:
          self.text_channel.send(text)
         else:
          logger.debug('Tried to send message but text channel not connected.')
	
	 luans.append(int(text))
	 
	 if (self.initialize):
    	  scoretime = time.time()
	  self.initialize=False


         if(len(luans)>(len(lans)-1)) and (self.gameover==True):
 	  global c1
	  c1=self.a1
	  global c2
	  c2=self.a2
	  gameComplete=True# If False the scorewindow displays "Incomplete Game".
   	  self.chatbox.add_text(self.owner,"Game Over")
	  accuracy=self.entry_ans_check_auto(luans)
   	  scoretime= time.time() - scoretime
	  self.gameover=False
	  self.write_score()
	  self.sort_score_file()
	  self.writedatatemp()
	  self.readdatatemp()
	  if full_alert:
	   self.report_of_data()
	   self.write_full_log()
	  entry.props.text = ''
	else:
	  self.chatbox.add_text(self.owner, strr)
          entry.props.text = ''


    def calAccuracy(self,st,m):
	a=((st-m)/float(st))*100
	a=int(a*100)/100.0 
	a=int(round(a,0))
	return a
       
    def entry_ans_check_ofothers(self,listl):
	 steps1=0
 	 no_of_mistake1=0
	 index1=0
	 global lans
	 while (index1 < len(listl)):
	   if not(lans[index1]==int(listl[index1])):
	    no_of_mistake1=no_of_mistake1+1
	   index1=index1+1
	   steps1=steps1+1
	 return self.calAccuracy(steps1,no_of_mistake1)
    #to evaluate the entered answer
    def entry_ans_check_auto(self,listl):
	 global steps
 	 global no_of_mistake
	 global accuracy
	 global lans
	 while (self.index < len(lans)):
	  if not (lans[self.index]==listl[self.index]):
	   no_of_mistake+=1
	  self.index=self.index+1
	  steps+=1
	 return self.calAccuracy(steps,no_of_mistake)

    def writedatatemp(self):
	with open('data1.txt','w+') as f1:
	  f1.write(self.chatbox.get_log())

    def readdatatemp(self):
	i=0
	j=0
	m=0
	n=0
	del self.a[:]

	while (i < len(self.buddylist)):
	 self.a.append(open('data_of_'+self.buddylist[i]+'.txt','w+'))
	 i=i+1

	while (j < len(self.buddylist)):
	 datalog= open('data1.txt','r+')
	 for line in datalog:
	  if self.buddylist[j] in line:
	    self.a[j].write(line)
    	 j=j+1

	while (m < len(self.a) and n < len(self.buddylist)):
	 self.a[m].seek(0)
	 datax=self.a[m].readlines()
	 #self.chatbox.add_text(self.owner,str(self.buddylist))
	 self.acc_time_of_calc(datax,self.buddylist[n])
	 m+=1
	 n+=1

    def acc_time_of_calc(self,ax,plname):
	l=[]
	curdate=time.strftime("%d/%m %I:%M")
	x=0#accuracy
	td=0.0#timeTaken
	firstp=30+len(plname)
	second=36
	ans=[]
	for line in ax:
	 al=line[firstp:]
	 al=al.replace('\n','')
	 if al.isdigit():
	  l.append(int(al))

	x = self.entry_ans_check_ofothers(l)#accuracy
	global lans
	global game_incomplete_players
	if not len(l) is len(lans):
	 game_incomplete_players.append(str(plname))
	global players
	players.append(plname)
	global accscores
	accscores.append(x)
	fline=ax[0]
	lline=ax[-1]
	t1=fline[11:26]
	t2= lline[11:26]
	t= datetime.datetime.strptime(t2,'%H:%M:%S:%f')-datetime.datetime.strptime(t1,'%H:%M:%S:%f')
	td = t.total_seconds()
	global timescores
	timescores.append(td)
	if full_alert:
	 with open("scoreStatisticsCSG.csv","a+") as f:
	  f.write(plname)
	  f.write(",")
	  f.write(curdate)
	  f.write(",")
	  f.write('%i\n'%x)
	 with open("timeeStatisticsCSG.csv","a+") as f:
	  f.write(plname)
	  f.write(",")
	  f.write(curdate)
	  f.write(",")
	  f.write('%.1f\n'%td)
	 self.write_report_of_data(plname,x)

    def write_report_of_data(self,username,score_of_user):
	 with open("report_data.csv","a+") as f:
	  f.write(username)
	  f.write(",")
	  f.write('%i\n'%score_of_user)

    def report_of_data(self):
	self.report_dict.clear()
	with open('report_data.csv') as f:
	    for line in f:
	        lbl, n = line.split(",")
	        n = int(n)
	        self.report_dict[lbl] = self.report_dict[lbl] + n
		
    def write_full_log(self):
	if ad:
	 line1='Addition'
	elif sb:
	 line1='Subtraction'
	if self.hardmode:
	 hardmodestr="Hard"
	else:
	 hardmodestr="Easy"
	
  	d_full={}
	global accscores
	global timescores
	global players
	for pl,ac,tm in zip(players,accscores,timescores):
	 d_full[pl]=ac,tm
	with open('full_data.txt','a+') as fi:
	 fi.write("\n=======================\n")
	 d1 = datetime.datetime.now()
	 d = d1.strftime("%d-%m-%Y %I:%M")
	 fi.write("\n"+d)
	 fi.write("\nFirst Num: "+str(self.a1)+"\tSecond Num: "+str(self.a2))
	 fi.write("\nMode  : "+line1 +"\t"+hardmodestr)
	 fi.write("\nDetails : "+str(d_full))
	 fi.write("\nIncomplete Players:\t"+str(game_incomplete_players)+"\n")

	 fi.write(self.chatbox.get_log())

    def write_score(self):
	if ad:
	 line1='Addition'
	elif sb:
	 line1='Subtraction'

     	l= [[accuracy,self.a1,self.a2,no_of_mistake,steps,'%.1f' % scoretime,line1]]
	with open('score_card.txt', 'a+') as f:
	 for row in l:
	    for column in row:
	        f.write('%s\t\t\t\t' % column)
	    f.write('\n')
	 f.close()



    def sort_score_file(self):
	with open('score_card.txt') as fin:
    		lines = [line.split() for line in fin]
	lines.sort(key=lambda x:int(x[0]), reverse=True)
	with open('score_card1.txt', 'w') as fout:
		for el in lines:
        		fout.write('{0}\t\t\t\t\n'.format('\t\t\t'.join(el)))

    def write_file(self, file_path):
        """Store chat log in Journal.

        Handling the Journal is provided by Activity - we only need
        to define this method.
        """
        logger.debug('write_file: writing %s' % file_path)
        self.chatbox.add_log_timestamp()
        f = open(file_path, 'w')
        try:
            f.write(self.chatbox.get_log())
        finally:
            f.close()
        self.metadata['mime_type'] = 'text/plain'
	self.metadata['GameTime']= '%.1f' % scoretime
	self.metadata['GameScore']=accuracy
	self.metadata['GameCreated']=str(datetime.datetime.now())


    def read_file(self, file_path):
        """Load a chat log from the Journal.

        Handling the Journal is provided by Activity - we only need
        to define this method.
        """
        logger.debug('read_file: reading %s' % file_path)	
	self.log = open(file_path).readlines()
                
	log = open(file_path).readlines()
        last_line_was_timestamp = False
        for line in log:
            if line.endswith('\t\t\n'):
                if last_line_was_timestamp is False:
                    timestamp = line.strip().split('\t')[0]
                    self.chatbox.add_separator(timestamp)
                    last_line_was_timestamp = True
            else:
                timestamp, nick, color, status, text = line.strip().split('\t')
                status_message = bool(int(status))
                self.chatbox.add_text({'nick': nick, 'color': color},
                              text, status_message)
                last_line_was_timestamp = False


class TextChannelWrapper(object):
    """Wrap a telepathy Text Channel to make usage simpler."""

    def __init__(self, text_chan, conn):
        """Connect to the text channel"""
        self._activity_cb = None
        self._activity_close_cb = None
        self._text_chan = text_chan
        self._conn = conn
        self._logger = logging.getLogger(
            'chat-activity.TextChannelWrapper')
        self._signal_matches = []
        m = self._text_chan[CHANNEL_INTERFACE].connect_to_signal(
            'Closed', self._closed_cb)
        self._signal_matches.append(m)

    def send(self, text):
        """Send text over the Telepathy text channel."""
        # XXX Implement CHANNEL_TEXT_MESSAGE_TYPE_ACTION
        if self._text_chan is not None:
            self._text_chan[CHANNEL_TYPE_TEXT].Send(
                CHANNEL_TEXT_MESSAGE_TYPE_NORMAL, text)

    def close(self):
        """Close the text channel."""
        self._logger.debug('Closing text channel')
        try:
            self._text_chan[CHANNEL_INTERFACE].Close()
        except Exception:
            self._logger.debug('Channel disappeared!')
            self._closed_cb()

    def _closed_cb(self):
        """Clean up text channel."""
        self._logger.debug('Text channel closed.')
        for match in self._signal_matches:
            match.remove()
        self._signal_matches = []
        self._text_chan = None
        if self._activity_close_cb is not None:
            self._activity_close_cb()

    def set_received_callback(self, callback):
        """Connect the function callback to the signal.

        callback -- callback function taking buddy and text args
        """
        if self._text_chan is None:
            return
        self._activity_cb = callback
        m = self._text_chan[CHANNEL_TYPE_TEXT].connect_to_signal('Received',
            self._received_cb)
        self._signal_matches.append(m)

    def handle_pending_messages(self):
        """Get pending messages and show them as received."""
        for identity, timestamp, sender, type_, flags, text in \
            self._text_chan[
                CHANNEL_TYPE_TEXT].ListPendingMessages(False):
            self._received_cb(identity, timestamp, sender, type_, flags, text)

    def _received_cb(self, identity, timestamp, sender, type_, flags, text):
        """Handle received text from the text channel.

        Converts sender to a Buddy.
        Calls self._activity_cb which is a callback to the activity.
        """
        if type_ != 0:
            # Exclude any auxiliary messages
            return

        if self._activity_cb:
            try:
                self._text_chan[CHANNEL_INTERFACE_GROUP]
            except Exception:
                # One to one XMPP chat
                nick = self._conn[
                    CONN_INTERFACE_ALIASING].RequestAliases([sender])[0]
                buddy = {'nick': nick, 'color': '#000000,#808080'}
            else:
                # Normal sugar MUC chat
                # XXX: cache these
                buddy = self._get_buddy(sender)
            self._activity_cb(buddy, text)
            self._text_chan[
                CHANNEL_TYPE_TEXT].AcknowledgePendingMessages([identity])
        else:
            self._logger.debug('Throwing received message on the floor'
                ' since there is no callback connected. See '
                'set_received_callback')

    def set_closed_callback(self, callback):
        """Connect a callback for when the text channel is closed.

        callback -- callback function taking no args

        """
        self._activity_close_cb = callback

    def _get_buddy(self, cs_handle):
        """Get a Buddy from a (possibly channel-specific) handle."""
        # XXX This will be made redundant once Presence Service
        # provides buddy resolution
        # Get the Presence Service
        pservice = presenceservice.get_instance()
        # Get the Telepathy Connection
        tp_name, tp_path = pservice.get_preferred_connection()
        conn = Connection(tp_name, tp_path)
        group = self._text_chan[CHANNEL_INTERFACE_GROUP]
        my_csh = group.GetSelfHandle()
        if my_csh == cs_handle:
            handle = conn.GetSelfHandle()
        elif group.GetGroupFlags() & \
            CHANNEL_GROUP_FLAG_CHANNEL_SPECIFIC_HANDLES:
            handle = group.GetHandleOwners([cs_handle])[0]
        else:
            handle = cs_handle

            # XXX: deal with failure to get the handle owner
            assert handle != 0

        return pservice.get_buddy_by_telepathy_handle(
            tp_name, tp_path, handle)
